#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/singleton.hpp>
#include <eosiolib/currency.hpp>


#define state_paid     0
#define state_verified 1
#define state_halted   2
#define state_finished 3

#define currency_USDT 0
#define currency_BTC  1
#define currency_ETH  2
#define currency_EOS  3


#define NEW_EVENT   1
#define FIRED_EVENT 2


#define EVENT_TYPE_DEAL   1
#define EVENT_TYPE_TRADE  2

 



/*1 existing user */
/*2 user equals system user */

#define onlyBe require_auth(sender); \
               eosio_assert(sender==N(alezozov), "you don't have permission to execute thus action");

 
typedef uint64_t ID; 

using namespace eosio;
using namespace std;

class membrana : public eosio::contract {
  public:
  
    static constexpr uint64_t code = N(membrana);
    static constexpr uint64_t TRANFARE_CURRENCY_NAME = S(4, SYS); // EOS
    
  
    membrana(account_name self)
      :
      contract(self),
      _this_contract(_self),
      _currency(_self),
      deals_tab(_self, _self),
      trades_tab(_self, _self){}
    
    
    /// @abi action
    void ping(account_name author) {
      eosio::print("pong");
    }
      
   
    /// @abi action
    void getstate(ID dealId) {
        
         //cleos push action alezozov getstate '[3]' -p alezozov
         
         auto d = deals_tab.find(dealId);

	   	 eosio_assert(d != deals_tab.end(), "no deal with this id");
         
         print(d->start); 
         
    }
    
    /// @abi action
    void getstart(ID dealId){
        
         //cleos push action alezozov getstart '[3]' -p alezozov
         
         auto d = deals_tab.find(dealId);

	   	 eosio_assert(d != deals_tab.end(), "no deal with this id");
         
         print(d->currentstate); 
         
    }
   
    // find deal or trade by id and mark it as event that was fired 
    
    /// @abi action
    void setverified(account_name sender,  ID dealId)  {
        
         //cleos push action alezozov setverified '["alezozov", 3]' -p alezozov
        
         onlyBe(sender);
         
         auto d = deals_tab.find(dealId);

	   	 eosio_assert(d != deals_tab.end(), "no deal with this id");
	   	 
	   	 eosio_assert(d->currentstate == state_paid, "deals state is not paid");

		 deals_tab.modify(d, sender, [&](auto& deal) {
		    deal.currentstate = state_verified;
		 });
         
         print("deal@", dealId, " was verefied"); 
         
    }
    
    /// @abi action
    void sethalted(account_name sender, ID dealId)  {
        
      //cleos push action alezozov sethalted '["alezozov", 3]' -p alezozov
      
      onlyBe(sender);
        
      auto d = deals_tab.find(dealId);

      eosio_assert(d != deals_tab.end(), "no deal with this id");
    
      eosio_assert((d->currentstate == state_paid || d->currentstate == state_verified), "deals state is not paid & not verified");
      eosio_assert(d->amount!=0, "error deal amount is 0");
      
      _transfereos(d->amount, d->trader);
    
      deals_tab.modify(d, sender, [&](auto& deal) {
           deal.amount = 0;
           deal.currentstate = state_halted;
      });
         
      print("deal@ ", dealId, " was halted");
     
    }
    
    
    /**
     *      EVENT SYSTEM    
     *      mark event (trade event or makedeal) as fired       
     * 
    */
    
    /// @abi action
    void setevntfired(account_name sender, ID id, ID eventType)  {
        
      //cleos push action alezozov setevntfired '["alezozov", 3, 2]' -p alezozov
      
      // only nodejs user can do this opertion
      
      onlyBe(sender);
      eosio_assert((eventType==EVENT_TYPE_DEAL || eventType==EVENT_TYPE_TRADE), "event type is not valid");
      
      
      if(eventType==EVENT_TYPE_DEAL){
          
          auto d = deals_tab.find(id);

          eosio_assert(d != deals_tab.end(), "no deal with this id");
        
          eosio_assert((d->event == NEW_EVENT), "this deal event was already fired");
          
          deals_tab.modify(d, sender, [&](auto& deal) {
               deal.event = FIRED_EVENT;
          });
             
          print("deal_event@ ", id, " was marked as fired");
          
      }
      
      else if(eventType==EVENT_TYPE_TRADE){
         
          auto t = trades_tab.find(id);

          eosio_assert(t != trades_tab.end(), "no trade with this id");
        
          eosio_assert((t->event == NEW_EVENT), "this trade event was already fired");
          
          trades_tab.modify(t, sender, [&](auto& trade) {
               trade.event = FIRED_EVENT;
          });
             
          print("trade_event@ ", id, " was marked as fired");
      }
     
    }
   
    ID getsplit(ID finishAmount, ID startBalance, ID targetBalance, ID amount)  {
        
      return ((finishAmount - startBalance) * amount) / ((targetBalance - startBalance) );
      
    }
     
    /**
     *      internal method
     *      send currency from contract account to some trader or investor
    */
    
    
    void _transfereos(ID amount, account_name to)  {
        
        
        extended_asset a;
        
        a.amount = amount;
        a.symbol = TRANFARE_CURRENCY_NAME; 
        a.contract = _this_contract;
        
        currency::inline_transfer( _this_contract, to, a , "transfare" );
        
        print("sending ", amount, " to ", to); 
        
    }
    
    /// @abi action
    void setfinished(account_name sender, ID dealId, ID finishAmount) {
       
         //cleos push action alezozov setfinished '["alezozov", 3, 200]' -p alezozov
       
         onlyBe(sender);
         
         auto d = deals_tab.find(dealId);

	   	 eosio_assert(d != deals_tab.end(), "no deal with this id");
	   	 eosio_assert(d->currentstate == state_verified, "deals state is not verefied");
         eosio_assert(d->amount!=0, "error deal amount is 0");
         
         
         if(finishAmount <= d->startbalance){
             
            _transfereos(d->amount, d->investor);
             
         }else if(finishAmount>d->targbalance){
             
            _transfereos(d->amount, d->trader);
            
         }
         else{
             
            ID split = getsplit(finishAmount, d->startbalance, d->targbalance, d->amount);
            
            _transfereos(split, d->trader);
            _transfereos(d->amount - split, d->investor);
            
         }
	   	  
         deals_tab.modify(d, sender, [&](auto& deal) {
             
            deal.amount = 0;
		    deal.currentstate = state_finished;
		    
		 });
         
         print(" - deal@ ", dealId, " was finished"); 
         
    }
       
    //getDealsCount
    
    /// @abi action
    void getdealscnt(account_name sender) {
        
        // cleos push action alezozov getdealscnt '["alezozov"]' -p alezozov
        
        ID deal_id = deals_tab.available_primary_key(); 
        print(deal_id);
        
    }
    
     /// @abi action
     void makedeal(
         account_name sender,
          
         ID duration, 
         ID maxloss, 
         ID startbalance, 
         ID targbalance, 
         ID amount, 
         
         // temp
         ID senteos,
         
         account_name  investor, 
         account_name  trader, 
       
         ID offer, 
         ID currency)
     {
            // compile contract:    /home/alezozov/c9sdk/membrana/contract# ./compile.sh alezozov membrana
            // unlock wallet:       cleos wallet unlock --password PW5KFa4RaJhY7qC8uFDZtEsDREdQLMzQV162Yg6YrQyMooFkrt
            // execute action:      cleos push action alezozov makedeal '["alezozov", 100, 200, 300, 400, 500, 500, "investor1", "trader" , 700, 3]' -p alezozov
            
            require_auth(sender);
            
            eosio_assert( ( currency >= 0 &&  currency < 4), "no such currency");
            
            eosio_assert( ( senteos ==  amount ), "not valid price");
        
            ID deal_id = deals_tab.available_primary_key();   
          
            deals_tab.emplace(sender, [&](auto& d) {
              
			      d.id = deal_id;  
				  d.currentstate =  state_paid;
                  d.start = now();
                  d.deadline = now() +  duration * 86400;
                  d.maxloss =  maxloss;
                  d.startbalance =  startbalance;
                  d.targbalance =  targbalance;
                  d.amount =  amount;
                  d.currency =  currency;
                  d.investor = investor;
                  d.trader =  trader;
                  d.event = NEW_EVENT;
			});
        
          
          // deposit coins from sender to membrana contract
          depositeos( sender, amount);
        
          print("deal@",  deal_id);
           
           
          
      }
      
      // addTradesBlock 
 
      /// @abi action
      void addtradesbl(account_name sender, ID hash) {
      
           //cleos push action alezozov addtradesbl '["alezozov", 3432434]' -p alezozov
           
           onlyBe(sender);
        
           ID trade_id = trades_tab.available_primary_key();   
          
           trades_tab.emplace(sender, [&](auto& d) {
              
	            d.id = trade_id;
	            d.hash = hash;  
	            d.event = NEW_EVENT;
		   });
        
           print("trade@",  trade_id);
           
    }
    
   
    /// @abi action
    void transfereos(account_name sender, ID amount, account_name to)  {
        
        //  cleos push action alezozov transfereos '["max", 3432434, "alezozov"]' -p alezozov
        
        onlyBe(sender);
        
        _transfereos( amount,  to);
       
    }
    
    /// @abi action
    void depositeos( account_name sender, ID amount)  {
        
        // cleos push action alezozov depositeos '[1, "max"]' -p alezozov
        
        require_auth(sender);
        
        extended_asset a;
        
        a.amount = amount;
        a.symbol = TRANFARE_CURRENCY_NAME; 
        a.contract = _this_contract;
        
        currency::inline_transfer( sender, _this_contract, a , "deposit" );
        
        print("depositing ", amount, " to ", _this_contract); 
       
    }
     
    // create coin and issue tokens for it
    /// @abi action
    void createcoin(account_name sender)  {
        
        //cleos push action alezozov createcoin '["max"]' -p alezozov
        
        onlyBe(sender);
        
        _currency.create_currency( { .issuer = _this_contract,
                                .maximum_supply = asset( 0, TRANFARE_CURRENCY_NAME ),
                                .issuer_can_freeze      = false,
                                .issuer_can_whitelist   = false,
                                .issuer_can_recall      = false } );

        // issue currency
        
        _currency.issue_currency( { .to =  _this_contract,
                                     .quantity          = asset( 10000000, TRANFARE_CURRENCY_NAME ),
                                     .memo              = string("initial membrana tokens") } );
        
        print("coin was created ");
       
    }
    
  private:
  
        account_name      _this_contract;
        currency _currency;
     
        // @abi table deals i64
        struct deals {		         
          
              ID  id;
              ID  currentstate;
              ID  start;
              ID  deadline;
              ID  maxloss;
              ID  startbalance;
              ID  targbalance;
              ID  amount;
              ID  currency;
              
              account_name  investor;
              account_name  trader;
                
              ID event;
        
              ID primary_key() const { return id; }
              ID get_event() const { return event; }
              
              EOSLIB_SERIALIZE(deals, (id)(currentstate)(start)(deadline)(maxloss)(startbalance)(targbalance)(amount)(currency)(investor)(trader)(event))
          
        }; 
        
        
        // @abi table trades i64
        struct trades {		         
          
              ID  id;
              ID  hash;
              ID event;  
        
              ID primary_key() const { return id; }
              ID get_event() const { return event; }
              
              EOSLIB_SERIALIZE(trades, (id)(hash)(event))
          
        };
        
        
        typedef //eosio::multi_index<N(deals), deals> deals_table;
        
        eosio::multi_index< N( deals ), deals, 
                indexed_by< N( byevnt ), const_mem_fun< deals, ID, &deals::get_event> >
                > deals_table;
        
        
      	deals_table deals_tab;
      	
      	typedef //eosio::multi_index<N(trades), trades> trades_table;
      	
      	eosio::multi_index< N( trades ), trades, 
                indexed_by< N( byevnt ), const_mem_fun< trades, ID, &trades::get_event> >
                > trades_table;
      	
      	trades_table trades_tab;
        
        typedef singleton<N(lstDealId), ID>                    lstDealId;    
        
        template <typename T>
        ID getSingletonValue(){ return T(code,_self).get_or_default(0); }
        
		    template <typename T>
        ID nextUniqeId(){
            ID lid =  getSingletonValue<T>() + 1;
            T(code, _self).set(lid, _self);
            return lid;
        }		
		 
};



// public contract actions 

EOSIO_ABI(membrana, (depositeos)  (createcoin) (ping) (makedeal) (getdealscnt) (addtradesbl) (setfinished) (sethalted) (setverified) (getstart) (getstate) (setevntfired))






