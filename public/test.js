var globals = {}

const chain_protocol = "http"

const chainId = "706a7ddd808de9fc2b8879904f3b392256c83104c1d544b38302cc07d9fca477";

globals.network = {
    blockchain: 'eos',
    host: '35.204.153.154', // ( or null if endorsed chainId )
    port: 8888, // ( or null if defaulting to 80 )
    //chainId: // Or null to fetch automatically ( takes longer )
}


const network_secure = globals.network

const requiredFields = {
    accounts: [globals.network],
};

globals.eosOptions = { chainId: chainId };


function eos_to_float(s) {
    //console.log('eos_to_float s=', s);
    var ret = s ? s.split(" ")[0] : 0;
    return parseFloat(ret);
}

document.addEventListener('scatterLoaded', scatterExtension => {
    console.log('scatterLoaded called');

    // if (active_block_producers.length == 0 && backup_block_producers.length == 0) {
    //     return;
    // }

    // Scatter will now be available from the window scope.
    // At this stage the connection to Scatter from the application is
    // already encrypted.
    globals.scatter = window.scatter;

    // It is good practice to take this off the window once you have
    // a reference to it.
    window.scatter = null;

    // If you want to require a specific version of Scatter
    var ret = globals.scatter.requireVersion(4.0);

    redrawAll();

})


setTimeout(() => {
    if (globals.scatter === null) {
        alert("can't find scatter extension")
    }
}, 2000);


function redrawAll() {
    // if (active_block_producers.length == 0 && backup_block_producers.length == 0)
    //     return;

    var eos = globals.scatter.eos(globals.network, EOS.Localnet, globals.eosOptions, chain_protocol);

    globals.scatter.suggestNetwork(globals.network).then((result) => {

        globals.scatter.getIdentity(requiredFields).then(identity => {

            // Set up any extra options you want to use eosjs with. 

            if (identity.accounts[0].authority != 'active') {

                alert('You have chosen an account with the ' + identity.accounts[0].authority + ' authority only the active authority can stake EOS. You should change identity');

                return;
            }


            // Get a reference to an 'Eosjs' instance with a Scatter signature provider.
            eos = globals.scatter.eos(network_secure, EOS.Localnet, globals.eosOptions, chain_protocol);

            eos.getAccount({ 'account_name': identity.accounts[0].name }).then((result) => {

                    console.log('getAccount result=', result);
                    //modal_stack.pop_entire_stack();

                    // Get our EOS balance
                    eos.getTableRows({
                        json: true,
                        code: 'eosio.token',
                        scope: identity.accounts[0].name,
                        table: 'accounts',
                        limit: 500
                    }).then(
                        (result) => {

                            const row = result.rows.find(row => row.balance.split(" ")[1].toLowerCase() === 'eos');
                            var balance = row ? row.balance.split(" ")[0] : 0;
                            console.log(balance)

                        }).catch(
                        (error) => {
                            alert('Scatter returned an error from getTableRows', error);
                            console.error('getTableRows error = ', error);
                        })

                    if (result.voter_info) {
                        // votes = result.voter_info.producers;
                        // proxy_name = result.voter_info.proxy;
                    }
                    else {
                        // votes = [];
                        // proxy_name = '';
                    }
                    if ( /*result.delegated_bandwidth == null || */ (eos_to_float(result.total_resources.cpu_weight) == 0 &&
                            eos_to_float(result.total_resources.net_weight) == 0)) {
                        // delegated_cpu_weight = '0';
                        // delegated_net_weight = '0';
                    }
                    else {
                        // delegated_cpu_weight = result ? result.total_resources.cpu_weight.split(" ")[0] : 0;
                        // delegated_net_weight = result ? result.total_resources.net_weight.split(" ")[0] : 0;
                    }


                })
                .catch((e) => {
                    console.error('Error returned by getAccount = ', e);

                });

        }).catch(error => {
            console.error('scatter.getIdentity() gave error=', error);

        });


    }).catch((error) => {

        console.error('Suggested network was rejected result=', error);

        if (error.type == "locked") {

            console.log(['Scatter is locked. Please unlock it and then retry']);

        }
        else {

            console.log('Scatter returned an error from suggestNetwork', error);

        }

    });
}
