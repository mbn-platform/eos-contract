#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/asset.hpp>

#define state_paid     0
#define state_verified 1
#define state_halted   2
#define state_finished 3

#define currency_USDT 0
#define currency_BTC  1
#define currency_ETH  2

#define onlyBe require_auth(sender);

 
typedef uint64_t ID; 
 

class membrana : public eosio::contract {
  public:
  
    membrana(account_name self)
      :eosio::contract(self){},
    deals_tab(_self, _self){},
    trades_tab(_self, _self){}
    
    
    
    /// @abi action
    void ping(account_name author) {
      eosio::print("pong4");
    }
      
    // /// @abi action 
    // ID getstate(ID dealId) public constant returns (uint)  {
    //   return uint(deals[dealId].currentState);
    // }
    
    // /// @abi action
    // ID getstart(ID dealId) public constant returns (uint)  {
    //   return deals[dealId].start;
    // }

    // /// @abi action
    // void setverified(account_name sender,  ID dealId)  {
    //      onlyBe(sender)
         
    //      //eosio_assert(deals[dealId].currentState == s);
    //      inState(dealId, state.paid)
         
         
    //      deals[dealId].currentState = state.verified;
    // }
    
    // /// @abi action
    // void sethalted(account_name sender, ID dealId)  {
      
    //   onlyBe(sender)
       
    //   require(deals[dealId].currentState == state.paid || deals[dealId].currentState == state.verified);
    //   require(deals[dealId].amount != 0);
       
    //   deals[dealId].traderAddress.transfer(deals[dealId].amount);
    //   deals[dealId].amount = 0;
    //   deals[dealId].currentState = state.halted;
     
    // }
    
  
    
    // /// @abi action
    // ID getsplit(ID finishAmount, ID startBalance, ID targetBalance, ID amount)  {
    //   return ((finishAmount - startBalance) * amount) / ((targetBalance - startBalance) );
    // }
      
    
    // /// @abi action
    // void setfinished(account_name sender, ID dealId, ID finishAmount) {
       
    //      onlyBe(sender)
    //      //eosio_assert(deals[dealId].currentState == s);
    //      inState(dealId, state.verified)
         
    //      require(deals[dealId].amount != 0);
         
    //      if(finishAmount <= deals[dealId].startBalance){
    //       deals[dealId].investorAddress.transfer(deals[dealId].amount);
    //      }else if(finishAmount>deals[dealId].targetBalance){
    //       deals[dealId].traderAddress.transfer(deals[dealId].amount);
    //      }
    //      else{
    //         uint split = getSplit(finishAmount, deals[dealId].startBalance, deals[dealId].targetBalance, deals[dealId].amount);
    //         deals[dealId].traderAddress.transfer(split);
    //         deals[dealId].investorAddress.transfer(deals[dealId].amount - split);
    //      }
    //      deals[dealId].amount = 0;
    //      deals[dealId].currentState = state.finished;
    // }
      
    
    // ID getdealscnt() {
    //     return deals.length;
    // }
    
   
    
     /// @abi action
     void makedeal(
         account_name sender,
          
         ID duration, 
         ID maxloss, 
         ID startbalance, 
         ID targbalance, 
         ID amount, 
         
         // temp
         ID senteos,
         
         string  investor, 
         string  investoraddr, 
         string  trader, 
         string  traderaddr, 
         ID amount,
         ID offer, 
         ID currency)
     {
        
         //cleos push action alezozov makedeal '[2,"user1","wireless mouse","mac magic Mouse","71.99 EOS"]' -p alezozov
        
        eosio_assert(! ( currency >= 0 &&  currency < 3), "no such currency");
        
        eosio_assert(! ( senteos ==  amount ), "not valid pice");
        
            ID deal_id = nextUniqeId<lstDealId>();   
          
            deals_tab.emplace(sender, [&](auto& d) {
			      d.id = deal_id;  
				  d.currentstate =  state_paid;
                  d.start = now();
                  d.deadline = now() +  duration * 86400;
                  d.maxloss =  maxloss;
                  d.startbalance =  startbalance;
                  d.targbalance =  targbalance;
                  d.amount =  amount;
                  d.currency =  currency;
                  d.investor = investor;
                  d.investoraddr =  investoraddr;
                  d.trader =  trader;
                  d.traderaddr =  traderaddr;
                  
			});
        
           
            
            //spawnInstance(msg.sender,deals.length-1, now, offer);
      }
      
      
   
    
    // /// @abi action
    // void addTradesBlock(account_name sender, ID hash) {
    //     onlyBe(sender)
    //     trades.push(hash);
    //     TradesBlock(hash);
    // }
    
    
     //address be = 0x873A2832898b17b5C12355769A7E2DAe6c2f92f7;
    
    // event TradesBlock(
    //     uint hash
    // );
    
  private:
    
        typedef eosio::multi_index<N(deals_tab), deals> deals_table;
    	deals_table deals_tab;
    	
    	typedef eosio::multi_index<N(trades_tab), trades> trades_table;
    	trades_table trades_tab;
    		  
        // @abi table deals i64
        struct deals {		         
          
              ID  id;
              ID  currentstate;
              ID  start;
              ID  deadline;
              ID  maxloss;
              ID  startbalance;
              ID  targbalance;
              ID  amount;
              ID  currency;
              string  investor;
              string  investoraddr;
              string  trader;
              string  traderaddr;
        
              ID primary_key() const { return id; }
              
              EOSLIB_SERIALIZE(deals, (id)(currentstate)(start)(deadline)(maxloss)(startbalance)(targbalance)(amount)(currency)(investor)(investoraddr)(trader)(traderaddr))
          
        }; 
        
        
        // @abi table trades i64
        struct trades {		         
          
              ID  id;
              ID  hash;
        
              ID primary_key() const { return id; }
              
              EOSLIB_SERIALIZE(trades, (id)(hash))
          
        };
        
        typedef singleton<N(lstDealId), ID>                    lstDealId;    
        
        template <typename T>
        ID getSingletonValue(){ return T(code,_self).get_or_default(0); }
        
		template <typename T>
        ID nextUniqeId(){
            ID lid =  getSingletonValue<T>() + 1;
            T(code, _self).set(lid, _self);
            return lid;
        }		
		 
};




EOSIO_ABI(membrana, (ping) (makedeal) /*(getdealscnt) (setfinished) (getsplit) (sethalted) (setverified) (getstart) (getstate)*/)


