import React from 'react'
import ReactDOM from 'react-dom'
import EOS from 'eosjs'
 
 

var globals = {}

const chain_protocol = "http"

const chainId = "cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f";

globals.network = {
    blockchain: 'eos',
    host: '35.204.153.154', // ( or null if endorsed chainId )
    port: 8888 // ( or null if defaulting to 80 )
}


const network_secure = globals.network

const requiredFields = {
  accounts: [globals.network],
};

globals.eosOptions = { chainId: chainId };


function eos_to_float(s) {
  //console.log('eos_to_float s=', s);
  var ret = s ? s.split(" ")[0] : 0;
  return parseFloat(ret);
}



/**
      
  using scatter https://github.com/EOSEssentials/Scatter#interacting-with-scatter
  
*/


// listen when scatter loads and init web app after this

document.addEventListener('scatterLoaded', scatterExtension => {

  console.log('scatterLoaded called');

  // Scatter will now be available from the window scope.
  // At this stage the connection to Scatter from the application is
  // already encrypted.
  globals.scatter = window.scatter;

  // It is good practice to take this off the window once you have
  // a reference to it.
  window.scatter = null;

  // If you want to require a specific version of Scatter
  var ret = globals.scatter.requireVersion(4.0);
  
  
   var eos = globals.scatter.eos(globals.network, EOS, globals.eosOptions, chain_protocol);

    globals.scatter.suggestNetwork(globals.network).then((result) => {

      globals.scatter.getIdentity(requiredFields).then(identity => {

        // Set up any extra options you want to use eosjs with. 

        if (identity.accounts[0].authority != 'active') {

          alert('You have chosen an account with the ' + identity.accounts[0].authority + ' authority only the active authority can stake EOS. You should change identity');

          return;
        }


        // Get a reference to an 'Eosjs' instance with a Scatter signature provider.
        eos = globals.scatter.eos(network_secure, EOS, globals.eosOptions, chain_protocol);

        // try to get account of scatter user
        
        eos.getAccount({ 'account_name': identity.accounts[0].name }).then((result) => {


            globals.accountName = identity.accounts[0].name;
            globals.identity = identity;
            globals.eos = eos;
            
            console.log('getAccount result=', result);
            
            /**
                init React app after everything worked with scatter
            */
            
            ReactDOM.render(<Dealdemo />, document.getElementById('app'));
            
            module.hot.accept();
            
              
            
          })
          .catch((e) => {
            console.error('Error returned by getAccount = ', e);

          });

      }).catch(error => {
        console.error('scatter.getIdentity() gave error=', error);

      });


    }).catch((error) => {

      console.error('Suggested network was rejected result=', error);

      if (error.type == "locked") {

        console.log(['Scatter is locked. Please unlock it and then retry']);

      }
      else {

        console.log('Scatter returned an error from suggestNetwork', error);

      }

    });



})


/**
 *    2 seconds check of scatter existanace in browser
*/

setTimeout(() => {
  if (globals.scatter === null) {
    alert("can't find scatter extension")
  }
}, 2000);

 
const EOS_CONFIG = {
    contractName: "alezozov", // Contract name
    contractSender: "alezozov", // User executing the contract (should be paired with private key)
}
 
class Dealdemo extends React.Component {

  constructor(props) {
    super(props)
    this.state = { 
      dealStatus: false ,
      transaction: false
    }
  }
  
  /**
    *    get membrana contract object
    *  
  */
            
  getMembranaContract(cb){

        globals.eos.contract(EOS_CONFIG.contractName)
        .then((contract) => {
          
            cb(contract)
           
        })
     
  }
  
  
  /**
   *    executed makedeal action of membrana contract
   *  
  */
            
   makeDeal(
               duration, 
               maxloss, 
               startbalance, 
               targbalance, 
               amount, 
               
               senteos,
               
               investor, 
               trader, 
             
               offer, 
               currency, 
               
               cb, err){
    
     
    
      this.getMembranaContract(function(membrana){
        
          membrana.makedeal(
            
               globals.accountName,
               duration, 
               maxloss, 
               startbalance, 
               targbalance, 
               amount, 
               senteos,
               investor, 
               trader, 
               offer, 
               currency, 
               
          { authorization: [/**use acount name from scatter identety*/ globals.accountName /*EOS_CONFIG.contractSender*/] })
          .then((res) => { 
              cb(res);
          })
          .catch((error) => {
              err(error);
          })
         
         
      }, err);
   
  }

  MakeDealThroughScatter (cb) {
                
      this.makeDeal(
          
             1, 
             2, 
             3, 
             4, 
             5, 
             
             5,
             
             globals.accountName, //investor
             "trader", // trader
           
             22, 
             2,  
             
         function(docs){
            console.log(docs)  
            cb(docs)
            
      }, function(err){
            console.log(err)
            cb(false, err)
      })
      
  
  }
  
  InvestorPay (cb) {
    
        var _ = this;
              
        this.getMembranaContract(function(membrana){
          
            membrana.depositeos(
                 globals.accountName,
                 100,
            { authorization: [/**use acount name from scatter identety*/ globals.accountName /*EOS_CONFIG.contractSender*/] })
            .then((res) => { 
              
              
              if(res){
                  _.setState({ dealStatus: "success", transaction:JSON.stringify(res) })  
              }
              
                
            })
            .catch((error) => {
                alert(error);
            })
            
             
        }, function(e){
            alert(e)
        });
  
  }


  createDeal() {

    var _ = this;

    this.MakeDealThroughScatter(function(res, err){
      
      if(res){
          _.setState({ dealStatus: "success", transaction:JSON.stringify(res) })  
      }
      
      else if(err){
          alert(JSON.stringify(err))
          _.setState({ dealStatus: "fail" })  
      }
      
      
    });

    _.setState({ dealStatus: 'loading' })

 

  }

  render() {
    if (!this.state.dealStatus) {
      return (
        
        <div>
        
          <button onClick={this.createDeal.bind(this)}>Create Deal</button>
          <br/>
          <button onClick={this.InvestorPay.bind(this)}>InvestorPay</button>
          
        </div>
        
      )
    }
    else if (this.state.dealStatus == "loading") {
      return (<span style={{ color: "gray" }}>Creating deal ...</span>)
    }
    else if (this.state.dealStatus == "success") {
      return (
        <div>
          <span style={{ color: "green" }}>
            transation was successful!  
          </span> 
          <br/>
          <b>Transaction:</b> 
          { this.state.transaction } 
        </div> );
    }
    else if (this.state.dealStatus == "fail") {
      return (<span style={{ color: "red" }}>Deal creation failed</span>)
    }
  }
}




// const EOS_CONFIG = {
//     contractName: "alezozov", // Contract name
//     contractSender: "alezozov", // User executing the contract (should be paired with private key)
//     // clientConfig: {
//     //   chainId: "706a7ddd808de9fc2b8879904f3b392256c83104c1d544b38302cc07d9fca477",
//     //   keyProvider: ['5KVXzYnCoHprf3tK5hGAn1fs9iTHvwfQtCpzRgya8So7mBLEWi3'], // Your private key
//     //   httpEndpoint: 'http://35.204.153.154:8888' // EOS http endpoint
//     // }
//   }


//let eosClient = EOS(EOS_CONFIG.clientConfig)

//let eosClient = scatter.eos(EOS_CONFIG.clientConfig)

// console.log(EOS.version)

// eosClient.contract(EOS_CONFIG.contractName)
//   .then((contract) => {
//     contract.ping(EOS_CONFIG.contractSender, { authorization: [EOS_CONFIG.contractSender] })
//       .then((res) => { console.log(res); this.setState({ pingStatus: 'success' }) })
//       .catch((err) => { this.setState({ pingStatus: 'fail' }); console.log(err) })
//   })

// var network = {
//   blockchain: 'eos',
//   host: '35.204.153.154', // ( or null if endorsed chainId )
//   port: 8888, // ( or null if defaulting to 80 )
//   chainId: "706a7ddd808de9fc2b8879904f3b392256c83104c1d544b38302cc07d9fca477" // Or null to fetch automatically ( takes longer )
// }
