const express = require('express')
const path = require('path')
const EOS = require('eosjs')
const app = express()



const EOS_CONFIG = {
    contractName: "alezozov", // Contract name
    contractSender: "alezozov", // User executing the contract (should be paired with private key)
    clientConfig: {
      chainId: "cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f",
      keyProvider: ['5KVXzYnCoHprf3tK5hGAn1fs9iTHvwfQtCpzRgya8So7mBLEWi3'], // Your private key
      httpEndpoint: 'http://localhost:8888' // EOS http endpoint
    }
}

let eosClient = EOS(EOS_CONFIG.clientConfig);

app.use(express.static('public'));
 
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
 
app.use((request, response, next) => {
  //console.log(request.headers)
  next()
})

app.use((request, response, next) => {
  request.chance = Math.random()
  next()
})

const table_deals = "deals"
const table_trades = "trades" 


const EVENT_TYPE_DEAL   = 1
const EVENT_TYPE_TRADE  = 2



/**
 *    returns all table records from blockchain or return record by primary id
 *  
*/

function getTableRows(from_id, table, cb, err, table_key){
  
     table_key = table_key || "id"
     
     var config =  {
          "json": true,
          "code":  EOS_CONFIG.contractName,
          "scope": EOS_CONFIG.contractName,
          "table": table , //"deals" 
          "table_key": table_key,
          //"lower_bound": from_id+"",
          //"upper_bound": (to_id)+"",
          "limit": -1
     }
     
     if(from_id){
         config.lower_bound = from_id
         config.upper_bound = from_id + 1
         config.limit = 1;
     }
        
     eosClient.getTableRows(
       config 
      ).then((res) => { 
        
            //response.json(res)
            //console.log(res.rows)
            
            if(from_id && res.rows){
                for(var r in res.rows){
                    r =  res.rows[r]
                    console.log(r.id)
                    if(r.id==from_id){
                        return cb(r);
                    }
                }
                return cb(null);
            }
            else {
                return cb(res.rows)   
            }
            
            
          
      })
      .catch((error) => {
        
            //response.json(err)
            err(error)
          
      })
  
}

/**
 *    returns all deals or returns only one deal speefied by id parameter
 *  
*/

function getDeals(from_id, cb, err){
  
    getTableRows(from_id, table_deals, cb, err , "id");
 
}

/**
 *    returns all trades or returns only one tade speefied by id parameter
 *  
*/
function getTrades(from_id, cb, err){
  
    getTableRows(from_id, table_trades, cb, err, "id");
 
}


/**
 *    returns state of the deal
 *  
*/

function getDealState(id, cb, err){
  
    getTableRows(id, table_deals, function(deal){
      
      if(deal){
          return cb(deal.currentstate)
      }
      
      return cb(null) 
      
    }, err);
 
}


/**
 *    returns start of the deal
 *  
*/

function getDealStart(id, cb, err){
  
    getTableRows(id, table_deals, function(deal){
      
      if(deal){
          return cb(deal.start)
      }
      
      return cb(null) 
      
    }, err);
 
}


/**
 *    executed setverified action of membrana contract
 *  
*/

function dealSetVerefied(id, cb, err){
  
    //cleos push action alezozov setverified '["alezozov", 3]' -p alezozov
  
    getMembranaContract(function(membrana){
      
        membrana.setverified(EOS_CONFIG.contractSender, id, { authorization: [EOS_CONFIG.contractSender] })
        .then((res) => { 
            cb(res);
        })
        .catch((error) => {
            err(error);
        })
       
       
    }, err);
 
}

/**
 *    executed setfinished action of membrana contract
 *  
*/

function dealSetFinished(id, finishAmount, cb, err){
  
    //cleos push action alezozov setfinished '["alezozov", 3, 200]' -p alezozov
  
    getMembranaContract(function(membrana){
      
        membrana.setfinished(EOS_CONFIG.contractSender, id, finishAmount, { authorization: [EOS_CONFIG.contractSender] })
        .then((res) => { 
            cb(res);
        })
        .catch((error) => {
            err(error);
        })
       
       
    }, err);
 
}

/**
 *    executed makedeal action of membrana contract
 *  
*/

function makeDeal(
             duration, 
             maxloss, 
             startbalance, 
             targbalance, 
             amount, 
             
             
             senteos,
             
             investor, 
             trader, 
             
             offer, 
             currency, 
             
             cb, err){
  
    //cleos push action alezozov makedeal '["alezozov", 100, 200, 300, 400, 500, 500, "investor1", "trader" , 700, 3]' -p alezozov
  
    getMembranaContract(function(membrana){
      
        membrana.makedeal(
          
             EOS_CONFIG.contractSender,
             
             duration, 
             maxloss, 
             startbalance, 
             targbalance, 
             amount, 
             senteos,
             investor, 
             trader, 
             offer, 
             currency, 
        { authorization: [EOS_CONFIG.contractSender] })
        .then((res) => { 
            cb(res);
        })
        .catch((error) => {
            err(error);
        })
       
       
    }, err);
 
}




function getMembranaContract(cb){
  
    eosClient.contract(EOS_CONFIG.contractName)
    .then((contract) => {
      
      cb(contract)
       
    })
 
}

 
//////////////////////////// 
//          API 
////////////////////////////



app.get('/', (request, response) => {
  
      
      getMembranaContract(function(membrana){
      
        membrana.ping(EOS_CONFIG.contractSender, { authorization: [EOS_CONFIG.contractSender] })
            .then((res) => { 
                  render(res)
            })
            .catch((error) => {
                 render(error)
            })
           
           
      }, function() {
          
      });
      
      
      function render(data){
         response.render("main", {
            data: JSON.stringify(data)
         });
      }
      
      
       
})


app.get('/get_deals', (request, response) => {
  
      var id = request.query.id;
      var to;
      console.log("id:", id)
      
      getDeals(id,  
         function(docs){
            response.json(docs)  
            
            
      }, function(err){
            response.json(err)
      })
       
})



app.get('/get_trades', (request, response) => {
  
      var id = request.query.id;
      var to;
      console.log("id:", id)
      
      getTrades(id,  
         function(docs){
            
            response.json(docs)  
            
      }, function(err){
            response.json(err)
      })
       
})




app.get('/get_deal_state', (request, response) => {
  
      var id = request.query.id;
      
      console.log("id:", id)
      
      getDealState(id,  
         function(state){
            response.json(state)  
      }, function(err){
            response.json(err)
      })
       
})



app.get('/get_deal_start', (request, response) => {
  
      var id = request.query.id;
      
      console.log("id:", id)
      
      getDealStart(id,  
         function(state){
            response.json(state)  
      }, function(err){
            response.json(err)
      })
       
})




app.get('/dealSetVerefied', (request, response) => {
  
      var id = request.query.id;
      console.log("id:", id)
      
      dealSetVerefied(
         id,  
         function(docs){
            response.json(docs)  
            
      }, function(err){
            response.json(err)
      })
       
})



app.get('/dealSetFinished', (request, response) => {
  
      var id = request.query.id;
      var finishAmount = request.query.id;
      console.log("id:", id)
      console.log("finishAmount:", finishAmount)
      
      dealSetFinished(
         id,  
         finishAmount, 
         function(docs){
            response.json(docs)  
            
      }, function(err){
            response.json(err)
      })
       
})


app.get('/makeDeal', (request, response) => {
     
       
       var duration = request.query.duration || 100;
       var maxloss = request.query.duration|| 200;
       var startbalance = request.query.startbalance|| 300;
       var targbalance= request.query.targbalance|| 400;
       var amount= request.query.amount|| 500;
       
       // temp
       var senteos = request.query.senteos || 500;
       
       var  investor = request.query.investor || "investor1";
       
       var  trader = request.query.trader|| "trader";
       
     
       var offer = request.query.offer|| 400;
       var currency = request.query.currency|| "2";
         
      
      console.log("id:", duration)
      
      makeDeal(
        
             duration, 
             maxloss, 
             startbalance, 
             targbalance, 
             amount, 
             
             senteos,
             
             investor, 
             trader, 
             
             offer, 
             currency,  
             
         function(docs){
            response.json(docs)  
            
      }, function(err){
            response.json(err)
      })
       
})




app.get('/ping', (request, response) => {
  
  
  getMembranaContract(function(membrana){
      
        membrana.ping(EOS_CONFIG.contractSender, { authorization: [EOS_CONFIG.contractSender] })
        .then((res) => { 
              response.json({
                chance: res
              })
        })
        .catch((error) => {
              response.json({
                chance: error
              })
        })
       
       
  }, function() {
      
  });
  
   
 
})



/**************************************************
 * 
 *      Event system 
 *    
 *      1 query contract for new deals or new trades
 *      2 if deal or trade was found mark that this event was fired so don't
 *        use it next time
 *      3 only system user can change event status
 * 
***************************************************/

const NEW_EVENT  =  1

function event_loop(table, cb) {
   
     var config =  {
          "json": true,
          "code":  EOS_CONFIG.contractName,
          "scope": EOS_CONFIG.contractName,
          "table": table ,  
          "table_key": "event",
          "lower_bound": NEW_EVENT + "",
          //"upper_bound": (NEW_EVENT + 1)+"",
          "limit": -1
     }
     
     function continue_event_loop(){
        // call event loop again
        setTimeout(function(){
              event_loop(table, cb);
        }, 1000);
     }
     
     eosClient.getTableRows(
          config 
      ).then((res) => { 
             
            // iterate through each event and process it with external function:
            
            
            if(res.rows && res.rows.length){
                
                //console.log("found events "+table+": ", res.rows.length)
                
                getMembranaContract(function(membrana){
                  
                    for(var r in res.rows){
                        
                        r =  res.rows[r]
                        
                        if( r.event != NEW_EVENT){
                            continue;
                        }
                        
                        // process event callback
                        cb(r)
                        
                        // mark each found event as done in contract
                        
                        //setevntfired(account_name sender, ID id, ID eventType)
                         
                        var event_type;
                        
                        if(table==table_deals){
                             event_type = EVENT_TYPE_DEAL;
                        }
                        
                        else if(table==table_trades){
                             event_type = EVENT_TYPE_TRADE;
                        }
                        
                        membrana.setevntfired(EOS_CONFIG.contractSender, r.id, event_type, { authorization: [EOS_CONFIG.contractSender] })
                        .then((res) => { 
                               
                        })
                        .catch((error) => {
                               
                        })
                        
                        
                    }
                    
                    continue_event_loop();
       
       
                }, function() {
                    continue_event_loop();
                });
                 
            }
            else{
                continue_event_loop();
            }
           
          
      })
      .catch((error) => {
         
            console.log(error)
            continue_event_loop();
          
      })
  
  
  
}


// run event loops:

event_loop(table_deals, function (deal){
    
    console.log("new deal was created:", deal)
    
});

event_loop(table_trades, function (trade){
    
    console.log("new trade was created:", trade)
    
});


app.listen(3000)


/**

eos.getTableRows()
USAGE
getTableRows - Fetch smart contract data from an account.

PARAMETERS
{
  "json": {
    "type": "bool",
    "default": false
  },
  "code": "name",
  "scope": "name",
  "table": "name",
  "table_key": "string",
  "lower_bound": {
    "type": "string",
    "default": "0"
  },
  "upper_bound": {
    "type": "string",
    "default": "-1"
  },
  "limit": {
    "type": "uint32",
    "default": "10"
  }
}

RETURNS
{
  "rows": {
    "type": "vector",
    "doc": "one row per item, either encoded as hex String or JSON object"
  },
  "more": {
    "type": "bool",
    "doc": "true if last element"
  }
}
*/
